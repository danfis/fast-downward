#ifndef TASKS_ROOT_TASK_H
#define TASKS_ROOT_TASK_H

#include "../abstract_task.h"
#include "../potentials/potential_function.h"

namespace tasks {
extern std::shared_ptr<AbstractTask> g_root_task;
extern std::vector<std::unique_ptr<potentials::PotentialFunction>> g_potentials;
extern void read_root_task(std::istream &in);
}
#endif
